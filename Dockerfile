############################################################
# Dockerfile para configurar aplicación en node.js - Express/GraphQL
############################################################
# To test locally run: docker build .
# Set the base image
FROM node:14

# Some Metadata information
LABEL "cl.apgca.appNode"="NODEJS_API_GRAPHQL"
LABEL maintainer="anelivaldes@gmail.com"
LABEL version="1.0"


# Create the folders for working directory
RUN mkdir -p /opt/graphqlserver

# Set the working directory
WORKDIR /opt/graphqlserver

# Instala los paquetes existentes en el package.json
COPY package.json .
RUN npm install --quiet

# Copy the applicarion sources
COPY . .

# Expose the port 8081
EXPOSE 8080

# Init the app
CMD [ "npm", "start"]
