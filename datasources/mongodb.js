const mongoose = require('mongoose');

export default function initMongoDB(callback) {
    const db = process.env.MONGO_DB;
    mongoose.connect(db, {useNewUrlParser: true, useUnifiedTopology: true});

    const mongoDB = mongoose.connection;

    mongoDB.on('error', (error) => {
        console.log(`Could not connect to MongoDB on : ${db}`);
        console.log(error);
        process.exit(1);
    });
    mongoDB.once('open', function () {
        console.log('Connected to MongoDB - ENV: ' +
            (process.env.NODE_ENV || 'default') +
            ` - DB: ${db}`);
        callback ? callback() : null;
    });
}
