<!-- GETTING STARTED -->
## A sample GraphQL Server

To get a local copy up and running follow these simple steps.

### Prerequisites

MongoBD:  

A mongo database is required, by default running on port 27017, to change that edit .env file (change MONGO_DB value)
There are several options:

1. You can have mongodb installed and running on your machine.[Install MongoDB](https://docs.mongodb.com/manual/installation/?jmp=footer)
 
2. Use a docker container. Install docker and then execute
  ```sh
  docker run --name mongo -d -p 27017:27017 mongo:latest
  ```
After that you will have a fresh copy of mongo running on 27017

  ```sh
  docker run --name mongo -d -p 27017:27017 mongo:latest
  ```
You can create a volume to persist the data for your container, like this:
  ```sh
docker run --name mongo-graphql -v /data/mongo-graphql:/etc/mongo -d -p 27017:27017 mongo:latest
```
Remember you may need to configure shared paths. [https://docs.docker.com/desktop/](https://docs.docker.com/docker-for-mac/)

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/anelivaldes/essentials.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```

### Try out
#### For development
 Start the projet
   ```sh
   npm start
   ```
Remenber you need to have a mongodb server running on port 27017.
#### Use Docker Compose
Another way is to use [docker-compose](https://docs.docker.com/compose/) to start the mongo database and the server with only one command. 

Before running the project with docker-compose, is required to edit the [.env](https://gitlab.com/anelivaldes/essentials/-/blob/master/.env) file and make an small change:
   ```sh
    MONGO_DB=mongodb://mongodb/graphqlserver
   ```

Try this:

   ```sh
    docker-compose up
   ```
If you made changes to the code you may need to rebuild the images, to get the updated version use:

   ```sh
docker-compose up --build
   ```
After the previous commands end you can check your environment like this:

   ```sh
docker ps -a
   ```
And the result will be similar to this:

   ```sh
CONTAINER ID   IMAGE               COMMAND                  CREATED          STATUS          PORTS                      NAMES
66e8952624db   graphqlserver:1.0   "docker-entrypoint.s…"   13 minutes ago   Up 13 minutes   0.0.0.0:8080->8080/tcp     graphqlserver
799ec74664d4   mongo               "docker-entrypoint.s…"   13 minutes ago   Up 13 minutes   0.0.0.0:27017->27017/tcp   mongo-graphql
   ```

### Playing with the server
Check http://localhost:8080/graphql in the browser. Click the button Docs to explore the api scheme. Can try these basic examples:

To add a book:

   ```sh
mutation{
    addBook(book: 
                {
                    title: "Jane Eyre", 
                    editorial: "Smith, Elder & Company", 
                    year: 1847
                },
            author: 
                {
                    name: "Charlotte Bronte ", 
                    nationality: "Inglesa"
                }
            )
            {
                id
                title
            }
}
   ```

To get all the books:

   ```sh
    {
      getAllBooks{
        title
        id
        author{
          name
        }
      }
    }
   ```
Also yo can use [Postman](https://www.postman.com/), and import and try the following collection [samples.postman_collection.json](https://gitlab.com/anelivaldes/essentials/-/blob/master/samples.postman_collection.json)
