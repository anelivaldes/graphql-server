import dotenv from 'dotenv';
import {join} from 'path';
import * as os from 'os';
import express from 'express';
const cors = require('cors');

import {graphqlHTTP} from 'express-graphql';
import {loadSchemaSync} from "@graphql-tools/load";
import {GraphQLFileLoader} from "@graphql-tools/graphql-file-loader";
import {addResolversToSchema} from "@graphql-tools/schema";

import initDBMongo from './datasources/mongodb';
import * as Mongoose from './mongoose';
import resolvers from "./graphql/resolvers";
import {logger} from './logger';

dotenv.config();
var bodyParser = require('body-parser');

const app = express();
app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use((req,res,next) => {
  logger.info(`${req.originalUrl} - ${req.method} - ${JSON.stringify(req.body)}`);
  next();
});

app.get('/', (req, res) => {
    res.send('Sample GraphQL Server');
});
// Load schema from the file
const schema = loadSchemaSync(join(__dirname, './graphql/schema.graphql'), {
    loaders: [
        new GraphQLFileLoader(),
    ]
});

// Add resolvers to the schema
const schemaWithResolvers = addResolversToSchema({
    schema,
    resolvers,
});

app.use('/graphql', graphqlHTTP({
    schema: schemaWithResolvers,
    graphiql: true,
    context: {Mongoose}
}));

initDBMongo(() => {
    const port = process.env.PORT;
    app.listen(8080, () => console.log(`Up and running in ${process.env.NODE_ENV ||
    'development'} @: ${os.hostname()} on port: ${port}`));
});
