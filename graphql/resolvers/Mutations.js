export default {
    addBook: async (root, {book, author}, {Mongoose}) => {
        const {title, year, editorial} = book;
        const b = new Mongoose.Book({title, year, editorial, author});
        // When the resolver make an asynchronous operation the resolver need to
        // returns a Promise that get resolved or rejected according to the
        return new Promise((resolve, reject) => {
            b.save(function (err, resp) {
                if (err) reject(err);
                else resolve(resp)
            })
        });
    },

    updateBook: async (root, {id, book, author}, {Mongoose}) => {
        const {title, year, editorial} = book;
        const updatedBook = await Mongoose.Book.findByIdAndUpdate(id, {
            title,
            year,
            editorial,
            author
        }, {new: true});

        if (!updatedBook) {
            throw new Error(`The book with the given ID ${id} was not found.`);
        } else return updatedBook;
    },

    deleteBookByTitle: async (root, {title}, {Mongoose}) => {
        const result = await Mongoose.Book.deleteOne({title});
        return result.deletedCount;
    },

    deleteBookById: async (root, {id}, {Mongoose}) => {
        const deletedBook = await Mongoose.Book.findByIdAndDelete(id);
        if (!deletedBook) {
            throw new Error(`The book with the given ID ${id} was not found.`);
        } else return deletedBook;
    }

}

