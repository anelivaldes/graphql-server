export default {
    getAllBooks: async (root, _, {Mongoose}) => {
        const books = await Mongoose.Book.find({});

        if (!books) {
            throw new Error(`No books was found.`);
        } else return books;
    },
    findBookById: async (root, {id}, {Mongoose}) => {
        const book = await Mongoose.Book.findById(id);
        if (!book) {
            throw new Error(`No books with the given id ${id} was found.`);
        } else {
            return book
        }
    },
    findBookByTitle: async (root, {title}, {Mongoose}) => {
        const books = await Mongoose.Book.find({title: {$regex: title, $options: 'i'}});
        if (!books) {
            throw new Error(`No books with the given title ${title} was found.`);
        } else {
            if (books.length > 0) {
                return books
            } else {
                throw new Error(`No books with the given title ${title} was found.`);
            }
        }
    },

    getAllAuthors: async (root, _, {Mongoose}) => {
        let allAuthors = await Mongoose.Book.find({}, 'author');
        if (!allAuthors) {
            throw new Error(`No books with the given title ${title} was found.`);
        } else {
            allAuthors = allAuthors.map(a => a._doc.author._doc);
            return Array.from(new Set(allAuthors.map(a => a.name))).map(name => {
                return allAuthors.find(a => a.name === name);
            });
        }

    }
}

