import Author from "./Author";
import Mutation from './Mutations';
import Query from './Query';

export default {
    Query,
    Mutation,
    Author
};
