export default {
    books: async (root, _, {Mongoose}) => {
        const authorName = root.name;
        const books = await Mongoose.Book.find({"author.name": authorName});

        if (!books) {
            throw new Error(`No books was found.`);
        } else return books;
    }
}

