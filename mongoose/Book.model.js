const mongoose = require('mongoose');
const AuthorSchema = new mongoose.Schema({
    name: String,
    nationality: String
});

const BookSchema = new mongoose.Schema({
    title: String,
    author: AuthorSchema,
    year: Number,
    editorial: String
});

const Book = mongoose.model('Book', BookSchema);
export {Book}
